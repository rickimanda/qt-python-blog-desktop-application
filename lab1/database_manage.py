import json
import datetime

# ----- architecture:
# class User:
#   userID, email, password - konstruktor, zapisywanie w jsonie
# class Blog:
#   blogID, title, ownerID, BlogItem[]
# class BlogItem
#   title, datetime, content

class User:
    def __init__(self, email, password):
        self.email = email
        self.password = password
        self.userID = None


    def add_to_db(self):
        with open(r"lab1\user_data.json",'r+') as file:
            # First we load existing data into a dict.
            file_data = json.load(file)

            self.userID = len(file_data["user_database"])

            userData ={
                "userID" : self.userID,
                "email" : self.email,
                "password" : self.password
            }

            # Join new_data with file_data inside emp_details
            file_data["user_database"].append(userData)
            # Sets file's current position at offset.
            file.seek(0)
            # convert back to json.
            json.dump(file_data, file, indent = 4)

            file.close()

class Blog:
    def __init__(self, title, ownerID):
        self.title = title
        self.ownerID = ownerID
        self.blogID = ownerID
        self.blogItem = []


    def add_to_db(self):
        with open(r"lab1\blog_data.json",'r+') as file:
            # First we load existing data into a dict.
            file_data = json.load(file)

            blogData ={
                "title" : self.title,
                "blogID" : self.ownerID,
                "ownerID" : self.ownerID,
                "blogItem" : []
            }

            # Join new_data with file_data inside emp_details
            file_data["blog_database"].append(blogData)
            # Sets file's current position at offset.
            file.seek(0)
            # convert back to json.
            json.dump(file_data, file, indent = 4)

            file.close()

    def edit_entry(self, index, title, content):
        with open(r"lab1\blog_data.json",'r+') as file:
            # First we load existing data into a dict.
                file_data = json.load(file)
                file.truncate(0)

                newEntry = BlogItem(title, content)
                entryData = BlogItem.get_item(newEntry)

                for elem in file_data["blog_database"]:      # for each elem in the list
                    if elem["ownerID"] == self["ownerID"]:
                        elem["blogItem"][index]["title"] = title
                        elem["blogItem"][index]["content"] = content

                # Sets file's current position at offset.
                file.seek(0)
                # convert back to json.
                json.dump(file_data, file, indent = 4)

                file.close()


    def add_entry(self, title, content):
            with open(r"lab1\blog_data.json",'r+') as file:
                # First we load existing data into a dict.
                file_data = json.load(file)

                newEntry = BlogItem(title, content)
                entryData = BlogItem.get_item(newEntry)

                for elem in file_data["blog_database"]:      # for each elem in the list
                    if elem["ownerID"] == self["ownerID"]:
                        elem["blogItem"].append(entryData)

                # Sets file's current position at offset.
                file.seek(0)
                # convert back to json.
                json.dump(file_data, file, indent = 4)

                file.close()


    def delete_entry(self, index):
        with open(r"lab1\blog_data.json",'r+') as file:
            # First we load existing data into a dict.
            file_data = json.load(file)
            file.truncate(0)

            for elem in file_data["blog_database"]:      # for each elem in the list
                if elem["ownerID"] == self["ownerID"]:
                        del elem["blogItem"][index]
                        break

            # Sets file's current position at offset.
            file.seek(0)
            # convert back to json.
            json.dump(file_data, file, indent = 4)

            file.close()

class BlogItem:
    def __init__(self, title, content):
        self.title = title
        self.content = content
        now = datetime.datetime.now()
        self.datetime = now.strftime("%Y-%m-%d %H:%M:%S")

    def get_item(self):
        itemData ={
            "title" : self.title,
            "content" : self.content,
            "datetime" : self.datetime
        }
        return itemData


def exists_user(email, password):
    with open(r"lab1\user_data.json",'r') as file:
        # First we load existing data into a dict.
        file_data = json.load(file)
        file.close()

        if not any( d["email"] == email and d["password"] == password for d in file_data["user_database"]):
            return False
        else: return True

def get_user(email):
    with open(r"lab1\user_data.json",'r') as file:
        # First we load existing data into a dict.
        file_data = json.load(file)
        file.close()

        for d in file_data["user_database"]:
            if d["email"] == email:
                return d

def get_blog(ownerID):
    with open(r"lab1\blog_data.json",'r') as file:
        # First we load existing data into a dict.
        file_data = json.load(file)
        file.close()

        for d in file_data["blog_database"]:
            if d["ownerID"] == ownerID:
                return d

def get_blog_entry(ownerID, index):
    with open(r"lab1\blog_data.json",'r') as file:
        # First we load existing data into a dict.
        file_data = json.load(file)
        file.close()

        for d in file_data["blog_database"]:
            if d["ownerID"] == ownerID:
                return d["blogItem"][index]






