import sys
from PySide6.QtCore import Qt, Slot, Signal
from PySide6.QtGui import QAction, QPainter, QFont, QFontInfo
from PySide6.QtWidgets import *
import widget_definitions
import database_manage

class blogWindow(QMainWindow):
    def __init__(self): # constructor without widget only has access to login dialog
        QMainWindow .__init__(self)
        self.setWindowTitle("EGUI PROJECT 1 - BLOG APPLICATION")
        self.centrWid = widget_definitions.blogPageWidget()
        self.setCentralWidget(self.centrWid)

        # keep info on which user is logged in
        self.loggedUser = None
        self.loggedUserBlog = None

        # show the login dialog when showing main window for the first time
        self.loginDialog = loginWindow()
        self.loginDialog.resize(600, 400)

        #declare also the edit dialog
        self.editDialog = editWindow()
        self.editDialog.resize(600, 400)

        # allow app to be closed from login dialog
        self.loginDialog.widget1.closeButton.clicked.connect(self.exit_app_from_login)

        # connect login button in dialog to function
        self.loginDialog.widget1.loginWidget1.loginButton.clicked.connect(self.login_user)

        # connect close button in this window to closing function
        self.centrWid.right1.closeButton.clicked.connect(self.exit_app)
        self.centrWid.right2.closeButton.clicked.connect(self.exit_app)

        # connect add entry button
        self.centrWid.right1.addEntryButton.clicked.connect(lambda: self.open_edit_dialog("add"))
        self.centrWid.right2.selectedEntryWidget.editButton.clicked.connect(lambda: self.open_edit_dialog("edit"))

        #connect buttons in the edit dialog to functions
        self.editDialog.widget1.cancelButton.clicked.connect(self.edit_cancel)
        self.editDialog.widget1.discardButton.clicked.connect(self.edit_discard)
        self.editDialog.widget1.saveButton.clicked.connect(self.edit_save)

    @Slot()
    def edit_save(self):
        short = self.editDialog.widget1

        # protection against empty fields
        if(short.titleEdit.text() == "" or short.contentEdit.toPlainText() == ""):
            short.messageLabel.setText("Title and content must not be ampty!")
            return

        # edit the entry
        database_manage.Blog.edit_entry(self.loggedUserBlog, self.centrWid.right1.blogEntriesList.currentRow(), short.titleEdit.text(), short.contentEdit.toPlainText())

        # close the window
        self.editDialog.close()

    @Slot()
    def edit_discard(self):
        # confirmation with a qmessagebox
        reply = QMessageBox.question(self, 'Discard', 'Are you sure you want to discard changes?',
                                     QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            # close the window
            self.editDialog.close()
        else:
            return


    @Slot()
    def edit_cancel(self):
        # confirmation with a qmessagebox
        reply = QMessageBox.question(self, 'Cancel', 'Are you sure you want to cancel adding a new entry?',
                                     QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            # delete the entry that was just created
            database_manage.Blog.delete_entry(self.loggedUserBlog, self.centrWid.right1.blogEntriesList.currentRow())
            # close the window
            self.editDialog.close()
        else:
            return


    @Slot()
    def open_edit_dialog(self, checked):
        short = self.editDialog.widget1

        # when adding an entry first create it and select it in the list
        if (checked == "add"):
            database_manage.Blog.add_entry(self.loggedUserBlog, None, None)
            self.centrWid.right1.blogEntriesList.setCurrentRow(self.centrWid.right1.blogEntriesList.count())
            short.discardButton.setEnabled(False)
            short.cancelButton.setEnabled(True)
            short.upperLabel.setText("Adding new entry")
        else:
            short.discardButton.setEnabled(True)
            short.cancelButton.setEnabled(False)

        # get entry from database
        currentRow = self.centrWid.right1.blogEntriesList.currentRow()
        selectedEntry = database_manage.get_blog_entry(self.loggedUser["userID"], currentRow)

        # fill
        short.titleEdit.setText(selectedEntry["title"])
        short.contentEdit.setText(selectedEntry["content"])

        # open edit dialog
        self.editDialog.exec()

        #do the update after closing dialog
        self.loggedUserBlog = database_manage.get_blog(self.loggedUser["userID"])
        self.centrWid.update_data(self.loggedUser, self.loggedUserBlog)

        # update the other thing also
        if self.centrWid.right1.blogEntriesList.count() !=0:
            selectedEntry = database_manage.get_blog_entry(self.loggedUser["userID"], currentRow)
            self.centrWid.right2.selectedEntryWidget.titleLabel.setText(selectedEntry["title"])
            self.centrWid.right2.contentBrowser.setText(selectedEntry["content"])


    @Slot()
    def login_user (self, checked):

        short = self.loginDialog.widget1.loginWidget1

        if (short.usernameInput.text() == "" or short.passwordInput.text() == ""):
            self.loginDialog.widget1.messageBox.setText("Enter login and password!")

        if (database_manage.exists_user(short.usernameInput.text(), short.passwordInput.text())):
            self.loginDialog.close()                                            #close login dialog
            self.loggedUser = database_manage.get_user(short.usernameInput.text())   #getlogged user data (dict)
            self.loggedUserBlog = database_manage.get_blog(self.loggedUser["userID"])     #get logged user's blog data (dict)
            self.centrWid.update_data(self.loggedUser, self.loggedUserBlog)               #call function to update/populate window
        else:
            self.loginDialog.widget1.messageBox.setText("Login failed! Email or password is wrong.")
            return

    @Slot()
    def exit_app(self, checked):
        sys.exit()

    @Slot()
    def exit_app_from_login(self, checked):
        sys.exit()


class loginWindow(QDialog):
    def __init__(self):
        QDialog.__init__(self)
        self.setWindowTitle("EGUI PROJECT 1 - BLOG APPLICATION - LOGIN")

        self.layout = QVBoxLayout()
        self.widget1 = widget_definitions.loginPageWidget()
        self.layout.addWidget(self.widget1)
        self.setLayout(self.layout)


class editWindow(QDialog):
    def __init__(self):
        QDialog.__init__(self)
        self.setWindowTitle("EGUI PROJECT 1 - BLOG APPLICATION - EDIT")

        self.layout = QVBoxLayout()
        self.widget1 = widget_definitions.editPageWidget()
        self.layout.addWidget(self.widget1)
        self.setLayout(self.layout)


if __name__ == "__main__":
    # Qt Application
    app = QApplication(sys.argv)

    window = blogWindow()
    window.resize(800, 600)
    window.show()
    window.loginDialog.exec()

    # Execute application
    sys.exit(app.exec())