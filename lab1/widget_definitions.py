import json
import sys

from PySide6.QtCore import Qt, Slot, QTimer, QObject, Signal, QEvent
from PySide6.QtGui import QAction, QFont, QFontInfo
from PySide6.QtWidgets import *
import database_manage

# ------------ login windwow ----------------

class loginWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        # prepare the login layout
        self.loginLayout = QVBoxLayout()

        # the "Log in" text
        self.loginText = QLabel("Login")
        self.loginText.setAlignment(Qt.AlignCenter)
        self.loginText.setFont( QFont( 'Segoe UI' , 12))
        self.loginLayout.addWidget(self.loginText)

        # username input
        self.usernameInput = QLineEdit()
        self.usernameInput.setPlaceholderText("Email")
        self.loginLayout.addWidget(self.usernameInput)

        # password input
        self.passwordInput = QLineEdit()
        self.passwordInput.setPlaceholderText("Password")
        self.passwordInput.setEchoMode(QLineEdit.Password)
        self.loginLayout.addWidget(self.passwordInput)

        # login button
        self.loginButton = QPushButton("Log In")
        self.loginLayout.addWidget((self.loginButton))

        # set layout for the widget
        self.setFixedSize(200, 125)
        self.setLayout(self.loginLayout)


class registerWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        # prepare the login layout
        self.loginLayout = QVBoxLayout()

        # the "Log in" text
        self.loginText = QLabel("Registration")
        self.loginText.setAlignment(Qt.AlignCenter)
        self.loginText.setFont( QFont( 'Segoe UI' , 12))
        self.loginLayout.addWidget(self.loginText)

        # username input
        self.usernameInput = QLineEdit()
        self.usernameInput.setPlaceholderText("Your email")
        self.loginLayout.addWidget(self.usernameInput)

        # password input
        self.passwordInput = QLineEdit()
        self.passwordInput.setPlaceholderText("Your Password")
        self.passwordInput.setEchoMode(QLineEdit.Password)
        self.loginLayout.addWidget(self.passwordInput)

        # repeat password input
        self.repeatPasswordInput = QLineEdit()
        self.repeatPasswordInput.setPlaceholderText("Repeat Password")
        self.repeatPasswordInput.setEchoMode(QLineEdit.Password)
        self.loginLayout.addWidget(self.repeatPasswordInput)

        # title of blog input
        self.blogTitleInput = QLineEdit()
        self.blogTitleInput.setPlaceholderText("Title of your blog")
        self.loginLayout.addWidget(self.blogTitleInput)

        # register button
        self.loginButton = QPushButton("Register")
        self.loginLayout.addWidget((self.loginButton))

        # set layout for the widget
        self.setFixedSize(200, 200)
        self.setLayout(self.loginLayout)


class loginPageWidget(QWidget): # main widget for the login window - everything here
    def __init__(self):
        QWidget.__init__(self)

        # Hbox for login and register widgets
        self.logRegLayout = QHBoxLayout()

        # add them
        self.loginWidget1 = loginWidget()
        self.registerWidget1 = registerWidget()
        self.logRegLayout.addWidget(self.loginWidget1)
        self.logRegLayout.addWidget(self.registerWidget1)
        self.logRegLayout.setAlignment(Qt.AlignCenter)

        # connect buttons to actions
        self.registerWidget1.loginButton.clicked.connect(self.register_user)

        # Hbox for help and close buttons
        self.helpCloseLayout = QHBoxLayout()

        # add them
        self.messageBox = QLabel("Register or log in to the blog above!")
        self.helpCloseLayout.addWidget((self.messageBox))
        self.closeButton = QPushButton("Close")
        self.helpCloseLayout.addWidget((self.closeButton))
        self.helpCloseLayout.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        # a label with app name
        self.titleLabel = QLabel("EGUI Project 1 - Blog application - Anna Krawczuk")
        self.titleLabel.setFixedHeight(40)
        self.titleLabel.setAlignment(Qt.AlignCenter)
        self.titleLabel.setFont( QFont( 'Segoe UI' , 16))

        # Vbox as main layout for this page/widget
        self.loginPageLayout = QVBoxLayout()
        self.loginPageLayout.addWidget(self.titleLabel, 1)
        self.loginPageLayout.addLayout(self.logRegLayout, 1)
        self.loginPageLayout.addLayout(self.helpCloseLayout)

        self.setLayout(self.loginPageLayout)

    @Slot()
    def register_user (self, checked):
        # check if fields are empty
        if (self.registerWidget1.passwordInput.text() == "" or \
                self.registerWidget1.usernameInput.text() == "" or
                self.registerWidget1.repeatPasswordInput.text() == "" or
                self.registerWidget1.blogTitleInput.text() == ""):
            self.messageBox.setText("Cannot register - some fields are empty.")
            return
        # check there is @ in email
        if '@' not in self.registerWidget1.usernameInput.text() :
            self.messageBox.setText("Cannot ergister - invalid email")
            return
        #check if password = repeat password
        if self.registerWidget1.passwordInput.text() != self.registerWidget1.repeatPasswordInput.text():
            self.messageBox.setText("Cannot register - passwords do not match.")
            return

        # call function to add to db
        newUser = database_manage.User(self.registerWidget1.usernameInput.text(), self.registerWidget1.passwordInput.text())
        database_manage.User.add_to_db(newUser)
        newBlog = database_manage.Blog(self.registerWidget1.blogTitleInput.text(), newUser.userID)
        database_manage.Blog.add_to_db(newBlog)

        # when done & do things
        self.messageBox.setText("Registration complete! You can log in now.")

        # clear fields and disable button for 1 second
        self.registerWidget1.usernameInput.clear()
        self.registerWidget1.passwordInput.clear()
        self.registerWidget1.repeatPasswordInput.clear()
        self.registerWidget1.blogTitleInput.clear()
        self.registerWidget1.loginButton.setDisabled(True)
        QTimer.singleShot(1000, lambda: self.registerWidget1.loginButton.setDisabled(False))


# ------------edit window ------------------

class editPageWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        self.editPageLayout = QVBoxLayout()
        self.setLayout(self.editPageLayout)

        self.upperLabel = QLabel("Entry editing")
        self.upperLabel.setFont( QFont( 'Segoe UI' , 14))
        self.editPageLayout.addWidget(self.upperLabel)

        self.titleLabel = QLabel("Title:")
        self.titleLabel.setFont( QFont( 'Segoe UI' , 12))
        self.editPageLayout.addWidget(self.titleLabel)

        self.titleEdit = QLineEdit()
        self.titleEdit.setPlaceholderText("Write the title of this blog entry here")
        self.editPageLayout.addWidget(self.titleEdit)

        self.contentLabel = QLabel("Contents:")
        self.contentLabel.setFont( QFont( 'Segoe UI' , 12))
        self.editPageLayout.addWidget(self.contentLabel)

        self.contentEdit = QTextEdit()
        self.contentEdit.setPlaceholderText("Write the contents of this blog entry here")
        self.editPageLayout.addWidget(self.contentEdit, 1)

        self.bottomLayout = QHBoxLayout()
        self.bottomLayout.addStretch()
        self.editPageLayout.addLayout(self.bottomLayout)

        self.messageLabel = QLabel("")
        self.bottomLayout.addWidget(self.messageLabel)

        self.cancelButton = QPushButton("Cancel")
        self.bottomLayout.addWidget(self.cancelButton)

        self.discardButton = QPushButton("Discard changes")
        self.bottomLayout.addWidget(self.discardButton)

        self.saveButton = QPushButton("Save changes")
        self.bottomLayout.addWidget(self.saveButton)


# --------------- blog window --------------

class blogEntryWidget(QWidget):
    def __init__(self, title, date):
        QWidget.__init__(self)

        # single entry in the list of blog entries

        self.blogEntryListLayout = QHBoxLayout()
        self.titleLabel = QLabel(title)
        self.titleLabel.setFont( QFont( 'Segoe UI' , 12))
        self.blogEntryListLayout.addWidget(self.titleLabel, 1)
        self.dateLabel = QLabel(date)
        self.blogEntryListLayout.addWidget(self.dateLabel)
        self.editButton = QPushButton("Edit")
        self.editButton.setVisible(False)
        self.blogEntryListLayout.addWidget(self.editButton)
        self.deleteButton = QPushButton("Delete")
        self.deleteButton.setVisible(False)
        self.blogEntryListLayout.addWidget(self.deleteButton)
        self.blogEntryListLayout.setSizeConstraint(QLayout.SetFixedSize)
        self.setLayout(self.blogEntryListLayout)

# I have to pack this into a widget so I can put it into a stacked layout later
class blogEntriesLayoutWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        # right side vertical layout - list of entries
        self.blogEntriesLayout = QVBoxLayout()

        self.addEntryButton = QPushButton("Add New Entry")
        self.addEntryButtonLayout = QHBoxLayout()
        self.addEntryButtonLayout.addWidget(self.addEntryButton)
        self.addEntryButtonLayout.addStretch()
        self.blogEntriesLayout.addLayout(self.addEntryButtonLayout)

        # the list
        self.blogEntriesList = QListWidget()
        self.blogEntriesLayout.addWidget(self.blogEntriesList, 1)

        self.closeButton = QPushButton("Close")
        self.closeButtonLayout = QHBoxLayout()
        self.closeButtonLayout.addStretch()
        self.closeButtonLayout.addWidget(self.closeButton)
        self.blogEntriesLayout.addLayout(self.closeButtonLayout)

        self.setLayout(self.blogEntriesLayout)

class blogWievLayoutWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        #different version of the right side layout - single entry viewing
        self.singleEntryLayout = QVBoxLayout()

        self.backButton = QPushButton("Back")
        self.backButtonLayout = QHBoxLayout()
        self.backButtonLayout.addWidget(self.backButton)
        self.backButtonLayout.addStretch()
        self.singleEntryLayout.addLayout(self.backButtonLayout)

        self.selectedEntryWidget = blogEntryWidget("blog_entry_title", "Blog_entry_date")
        self.singleEntryLayout.addWidget(self.selectedEntryWidget)

        self.contentBrowser = QTextBrowser()
        self.singleEntryLayout.addWidget(self.contentBrowser, 1)

        self.closeButton = QPushButton("Close")
        self.closeButtonLayout = QHBoxLayout()
        self.closeButtonLayout.addStretch()
        self.closeButtonLayout.addWidget(self.closeButton)
        self.singleEntryLayout.addLayout(self.closeButtonLayout)

        self.setLayout(self.singleEntryLayout)

class blogPageWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        # left side vertical layout - blog info
        self.blogInfoLayout = QVBoxLayout()
        self.titleLabel = QLabel("Blog Title")
        self.titleLabel.setFont( QFont( 'Segoe UI' , 16))
        self.blogInfoLayout.addWidget(self.titleLabel)
        self.blogInfoLayout.addWidget(QLabel("by"))
        self.usernameLabel = QLabel("User's email")
        self.usernameLabel.setFont( QFont( 'Segoe UI' , 14))
        self.blogInfoLayout.addWidget(self.usernameLabel)
        self.numberLabel = QLabel("Number of entries: ")
        self.blogInfoLayout.addWidget(self.numberLabel)
        self.blogInfoLayout.addStretch()

        # so the satcked layout for the right side
        self.rightSideLayout = QStackedLayout()
        self.right1 = blogEntriesLayoutWidget()
        self.right2 = blogWievLayoutWidget()
        self.rightSideLayout.addWidget(self.right1)
        self.rightSideLayout.addWidget(self.right2)
        self.rightSideLayout.setCurrentWidget(self.right1)

        #main layout - combine the above
        self.blogPageLayout = QHBoxLayout()
        self.blogPageLayout.addLayout(self.blogInfoLayout)
        self.blogPageLayout.addLayout(self.rightSideLayout, 1)

        self.setLayout(self.blogPageLayout)

        # buttton functionalities
        self.right1.blogEntriesList.itemActivated.connect(lambda: self.change_layout(self.right1.blogEntriesList.currentRow() ))
        self.right2.backButton.clicked.connect(self.change_layout_back)
        self.right2.selectedEntryWidget.deleteButton.clicked.connect(lambda: self.delete_entry(self.right1.blogEntriesList.currentRow() ))

    @Slot()
    def change_layout(self, index):
        # get entry from database
        loggedUser = database_manage.get_user(self.usernameLabel.text())
        selectedEntry = database_manage.get_blog_entry(loggedUser["userID"], index)

        # fill the fields
        self.right2.selectedEntryWidget.titleLabel.setText(selectedEntry["title"])
        self.right2.selectedEntryWidget.dateLabel.setText(selectedEntry["datetime"])
        self.right2.contentBrowser.setText(selectedEntry["content"])
        self.right2.selectedEntryWidget.editButton.setVisible(True)
        self.right2.selectedEntryWidget.deleteButton.setVisible(True)

        # set the layout
        self.rightSideLayout.setCurrentWidget(self.right2)

    @Slot()
    def change_layout_back(self):
        loggedUser = database_manage.get_user(self.usernameLabel.text())
        loggedBlog = database_manage.get_blog(loggedUser["userID"])
        self.update_data(loggedUser, loggedBlog)
        self.right2.selectedEntryWidget.editButton.setVisible(True)
        self.right2.selectedEntryWidget.deleteButton.setVisible(True)

        self.rightSideLayout.setCurrentWidget(self.right1)


    @Slot()
    def delete_entry(self, index):
        reply = QMessageBox.question(self, 'Delete',
                                     'Are you sure you want to delete this entry?',
                                     QMessageBox.Yes | QMessageBox.No, QMessageBox.No)

        if reply == QMessageBox.Yes:
            loggedUser = database_manage.get_user(self.usernameLabel.text())
            loggedBlog = database_manage.get_blog(loggedUser["userID"])
            database_manage.Blog.delete_entry(loggedBlog, index)
            self.change_layout_back()


    @Slot(int)
    def update_data(self, user, blog):
        self.titleLabel.setText(blog["title"])
        self.usernameLabel.setText(user["email"])
        self.numberLabel.setText("Number of entries: " + str(len(blog["blogItem"])))
        self.right1.blogEntriesList.clear()

        for entry in blog["blogItem"]:
            newEntry = blogEntryWidget(entry["title"], entry["datetime"])
            item = QListWidgetItem()
            item.setSizeHint(newEntry.sizeHint())
            self.right1.blogEntriesList.addItem(item)
            self.right1.blogEntriesList.setItemWidget(item, newEntry)







